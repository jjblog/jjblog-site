import jinja2
import json

class ProjectUrl:
    def __init__(self, url_json):
        self.url = url_json['url']
        self.name = url_json['name']

class Day:
    def __init__(self, day_json):
        self.paragraphs = []
        self.commits = []
        self.paragraphs = day_json['paragraphs']
        self.commits = day_json['commits']

    def __str__(self):
        string_to_return = ""
        string_to_return += "Paragraphs:\n"
        for paragraph in self.paragraphs:
            string_to_return += "\t" + paragraph + "\n"
        string_to_return += "Commits:\n"
        for commit in self.commits:
            string_to_return += "\t" + commit + "\n"
        return string_to_return


class Project:
    def __init__(self, project_json):
        self.urls = []
        self.days = []
        self.blog_url = project_json['blogUrl']
        self.title = project_json['title']
        self.description = project_json['description']
        self.project_file = project_json['projectFile']
        for url in project_json['urls']:
            self.urls.append(ProjectUrl(url))
        with open(self.project_file) as project_detail_json:
            data = json.load(project_detail_json)
            for day in data['days']:
                self.days.append(Day(day))

    def __str__(self):
        string_to_return = ""
        string_to_return += self.title + "\n"
        string_to_return += "\tDays:\n"
        for day in self.days:
            string_to_return += "\t" + str(day)
        return string_to_return

def parseHomePageJson():
    projects = []
    with open("projects/index.json") as json_index_file:
        data = json.load(json_index_file)
        for project in data['projects']:
            projects += [Project(project)]
    return projects

projects = parseHomePageJson()

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_INDEX = "templates/index.html"
TEMPLATE_PROJECT = "templates/project.html"
template_index = templateEnv.get_template(TEMPLATE_INDEX)
template_project = templateEnv.get_template(TEMPLATE_PROJECT)

index_html = template_index.render(projects=projects)
with open("build/index.html", "w") as index_file:
    index_file.write(index_html)

for project in projects:
    project_html = template_project.render(project=project)
    with open("build/"+project.blog_url, "w") as project_file:
        project_file.write(project_html)


